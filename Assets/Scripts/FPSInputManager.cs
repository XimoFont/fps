﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{

    private PlayerMovement playerController;
    private PlayerMovement playerController2;
    private float sensitivity = 3.0f;
    private LookRotation lookRotation;
    private Vector2 inputAxis;
    private Vector2 mouseAxis;
    private Laser playerLaser;
    private MouseCursor mouseCursor;
    private BallShot ballShot;
    private RocketShot rocketShot;
    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        playerController2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<PlayerMovement>();
        lookRotation = playerController.GetComponent<LookRotation>();
        playerLaser = playerController.GetComponent<Laser>();
        ballShot = playerController.GetComponent<BallShot>();
        rocketShot = playerController.GetComponent<RocketShot>();
        mouseCursor = new MouseCursor();
        mouseCursor.HideCursor();
    }

    void Update()
    {
        //El movimiento del player
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");
        playerController.SetAxis(inputAxis);
        playerController2.SetAxis(inputAxis);
        //El salto del player
        if(Input.GetButton("Jump")) playerController.StartJump();

        //Rotación de la cámara
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        //Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
        lookRotation.SetRotation(mouseAxis);

        if(Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
        else if(Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();

        if(Input.GetMouseButtonDown(0)) playerLaser.Shot();
        if(Input.GetKeyDown(KeyCode.R)) playerLaser.Reload();
        if(Input.GetMouseButtonDown(1)) ballShot.Shot();
        if(Input.GetMouseButtonDown(2)) rocketShot.Shot();
    }
}