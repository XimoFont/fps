﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationEvents : MonoBehaviour
{

    EnemyBehaviour Enemy;
    private void Start()
    {
        Enemy = GetComponentInParent<EnemyBehaviour>();
    }
    public void EndExplosion()
    {
        Debug.Log("Explosion!!!");
        Enemy.Explosion();
    }
}
