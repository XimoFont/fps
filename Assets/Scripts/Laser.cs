﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{

    public float maxDistance;
    public LayerMask mask;

    public int maxAmmo;
    public int currentAmmo;
    public float fireRate;
    public float hitForce;
    public float hitDamage;

    public bool isShooting;
    public bool isReloading;

    public float reloadTime;

    private LineRenderer laserLine;

    private void Start()
    {
        isShooting = false;
        isReloading = false;
        currentAmmo = maxAmmo;
        laserLine = GetComponent<LineRenderer>();
    }

    public void Shot()
    {
        if(isShooting || isReloading) return;
        if(currentAmmo <= 0) return;

        Debug.Log("Shoot");

        isShooting = true;
        currentAmmo--;

        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, 0.0f));

        // Set the start position for our visual effect for our laser to the position of gunEnd
        laserLine.enabled = true;
        laserLine.SetPosition(0, transform.position);

        RaycastHit hit;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit, maxDistance, mask))
        {
            Debug.Log("Hit");
            Debug.Log(hit.transform.name);
            hit.rigidbody.AddForce(Camera.main.transform.forward * hitForce);
            laserLine.SetPosition(1, hit.transform.position);
        }
        else
        {
            laserLine.SetPosition(1, transform.forward * maxDistance);
        }

        StartCoroutine(WaitFireRate());
    }
    private IEnumerator WaitFireRate()
    {/*
        float timeCounter = 0;
        while(timeCounter < fireRate)
        {
            timeCounter += Time.deltaTime;
        }
        isShooting = false;
 
        yield return null;*/
        Debug.Log("Empieza la corutina");
        yield return new WaitForSeconds(fireRate);
        isShooting = false;
        laserLine.enabled = false;
        Debug.Log("Termina la corutina");
    }

    public void Reload()
    {
        if(isReloading) return;
        isReloading = true;

        StartCoroutine(WaitForReload());
    }
    private IEnumerator WaitForReload()
    {
        yield return new WaitForSeconds(reloadTime);

        currentAmmo = maxAmmo;
        isReloading = false;
    }
}