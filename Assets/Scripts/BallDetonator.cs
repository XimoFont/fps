﻿using UnityEngine;
using System.Collections;

public class BallDetonator : MonoBehaviour
{

    float lifespan = 3.0f;
    public GameObject fireEffect;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(ExplodeCoroutine());
    }

    // Update is called once per frame
    /*
    void Update () {
        lifespan -= Time.deltaTime;
       
        if(lifespan <= 0) {
            Explode();
        }
    }
    */

    IEnumerator ExplodeCoroutine()
    {
        yield return new WaitForSeconds(lifespan);
        Explode();
    }

    void OnCollisionEnter(Collision collision)
    {

        if(collision.gameObject.tag == "Enemy")
        {
            //collision.gameObject.tag = "Untagged";
            GameObject fire = Instantiate(fireEffect, transform.position, Quaternion.identity);
            fire.AddComponent<RemoveExplosion>();
            Destroy(gameObject);
        }
    }

    void Explode()
    {

        Destroy(gameObject);
    }
}
