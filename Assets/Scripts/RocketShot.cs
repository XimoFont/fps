﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketShot : MonoBehaviour
{

    public GameObject bullet_prefab;
    float bulletImpulse = 200f;


    // Update is called once per frame
    public void Shot()
    {
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Acceleration);
    }
}
